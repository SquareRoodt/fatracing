//
//  WinnerTableViewCell.swift
//  FatRacing
//
//  Created by Jan-Dawid on 14/3/19.
//  Copyright © 2019 JD. All rights reserved.
//

import UIKit

class WinnerTableViewCell: UITableViewCell {
    
    static let cellHeight: CGFloat = Utils.isIpad() ? 120 : 80
    let profilePicture = UIImageView()
    
    let midLine = UIView()
    let graph = UIView()
    let winLine = UIView()
    let loseLine = UIView()
    let lineInset: CGFloat = 20
    let lineMax: Double = 8.0

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupBaseView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setupBaseView() {
        
        // ProfilePicture
        self.addSubview(profilePicture)
        profilePicture.frame = CGRect(x: 0, y: 5, width: WinnerTableViewCell.cellHeight-10, height: WinnerTableViewCell.cellHeight-10)
        
        // Line Graph
        graph.clipsToBounds = true
        self.addSubview(graph)
        graph.snp.makeConstraints { (make) -> Void in
            make.left.equalToSuperview().offset(WinnerTableViewCell.cellHeight + 10)
            make.top.right.equalToSuperview()
            make.height.equalTo(WinnerTableViewCell.cellHeight)
        }
        
        // Mid Line
        midLine.backgroundColor = UIColor.flatBlack()
        graph.addSubview(midLine)
        midLine.snp.makeConstraints { (make) -> Void in
            make.centerX.equalToSuperview()
            make.width.equalTo(1)
            make.top.bottom.equalToSuperview().inset(2)
        }
        
        // Win Line
        winLine.backgroundColor = UIColor.flatGreen()
        graph.addSubview(winLine)
        winLine.snp.makeConstraints { (make) -> Void in
            make.left.equalTo(midLine)
            make.top.bottom.equalToSuperview()
            make.width.equalTo(0)
        }
        
        // Lose Line
        loseLine.backgroundColor = UIColor.flatRed()
        graph.addSubview(loseLine)
        loseLine.snp.makeConstraints { (make) -> Void in
            make.right.equalTo(midLine)
            make.top.bottom.equalToSuperview()
            make.width.equalTo(0)
        }
    }
    
    func setupWith(image: UIImage?, win: Bool, amount: Int) {
        profilePicture.image = image
        
        self.setNeedsLayout()
        if win {
            winLine.snp.remakeConstraints { (make) -> Void in
                make.left.equalTo(midLine.snp.right)
                make.top.bottom.equalToSuperview().inset(lineInset)
                make.width.equalToSuperview().multipliedBy(Double(amount)/lineMax/2.0)
            }
            loseLine.snp.remakeConstraints { (make) -> Void in
                make.right.equalTo(midLine.snp.left)
                make.top.bottom.equalToSuperview().inset(lineInset)
                make.width.equalTo(0)
            }
        } else {
            winLine.snp.remakeConstraints { (make) -> Void in
                make.left.equalTo(midLine.snp.right)
                make.top.bottom.equalToSuperview().inset(lineInset)
                make.width.equalTo(0)
            }
            loseLine.snp.remakeConstraints { (make) -> Void in
                make.right.equalTo(midLine.snp.left)
                make.top.bottom.equalToSuperview().inset(lineInset)
                make.width.equalToSuperview().multipliedBy(Double(amount)/lineMax/2.0)
            }
        }
        self.layoutIfNeeded()
    }

}
