//
//  NewPlayerViewController.swift
//  FatRacing
//
//  Created by Jan-Dawid on 25/2/19.
//  Copyright © 2019 JD. All rights reserved.
//

import UIKit
import RSKImageCropper
import MobileCoreServices

class NewPlayerViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, PlayerEditCellDelegate, RSKImageCropViewControllerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    let pictureButton = UIButton()
    let tableView = UITableView()
    let saveButton = UIButton()
    var odds = [Int]()
    var isLoadedUser: Bool = false
    var loadedUserNum: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        for _ in Utils.shared.allPrizes {
            odds.append(1)
        }
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tableView.reloadData()
    }
    
    func loadPlayer(_ player: Player) {
        odds = player.goodOdds
        pictureButton.setImage(player.image, for: .normal)
        isLoadedUser = true
        loadedUserNum = player.playerID
        self.odds.removeAll()
        for odd in player.goodOdds {
            if odd == Player.star1Odds {
                self.odds.append(1)
            } else if odd == Player.star2Odds {
                self.odds.append(2)
            } else if odd == Player.star3Odds {
                self.odds.append(3)
            } else {
                self.odds.append(1)
            }
        }
        self.tableView.reloadData()
    }
    
    func setupView() {
        self.view.backgroundColor = .white
        
        // Back Button
        let backButton = UIButton()
        backButton.setBackgroundImage(UIImage(named: "BackButton"), for: .normal)
        backButton.addTarget(self, action: #selector(backPressed), for: .touchUpInside)
        self.view.addSubview(backButton)
        backButton.snp.makeConstraints { (make) -> Void in
            make.top.equalToSuperview().offset(20)
            make.left.equalToSuperview().offset(20)
            make.width.height.equalTo(60)
        }
        
        // Save Button
        saveButton.backgroundColor = UIColor.flatGreen()
        saveButton.setTitleColor(UIColor.white, for: .normal)
        saveButton.setTitle("Save", for: .normal)
        saveButton.titleLabel?.font = UIFont.systemFont(ofSize: 25, weight: .bold)
        saveButton.addTarget(self, action: #selector(savePlayer), for: .touchUpInside)
        saveButton.layer.cornerRadius = 25
        saveButton.clipsToBounds = true
        saveButton.isEnabled = isLoadedUser ? true : false
        self.view.addSubview(saveButton)
        saveButton.snp.makeConstraints { (make) -> Void in
            make.right.equalToSuperview().inset(30)
            make.top.equalTo(backButton)
            make.width.equalTo(120)
            make.height.equalTo(50)
        }
        
        // Profile Picture
        let w: CGFloat = 120
        pictureButton.backgroundColor = UIColor.flatGreen()
        pictureButton.layer.cornerRadius = w/2.0
        pictureButton.clipsToBounds = true
        pictureButton.setTitle("+", for: .normal)
        pictureButton.titleLabel?.font = UIFont.systemFont(ofSize: 30, weight: .heavy)
        pictureButton.addTarget(self, action: #selector(cameraPressed), for: .touchUpInside)
        self.view.addSubview(pictureButton)
        pictureButton.snp.makeConstraints { (make) -> Void in
            make.top.equalToSuperview().offset(20)
            make.width.height.equalTo(w)
            make.centerX.equalToSuperview()
        }
        
        // TableView
        tableView.register(PlayerEditCell.self, forCellReuseIdentifier: "cellName2")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.showsVerticalScrollIndicator = false
        self.view.addSubview(tableView)
        tableView.snp.makeConstraints { (make) -> Void in
            make.left.right.bottom.equalToSuperview().priority(.medium)
            make.centerX.equalToSuperview()
            make.width.lessThanOrEqualTo(PlayerEditCell.cellMaxWidth).priority(.high)
            make.top.equalTo(pictureButton.snp.bottom).offset(20)
        }
    }
    
    @objc func savePlayer() {
        let playerNum = isLoadedUser ? loadedUserNum : Utils.shared.allPlayers.count
        if let img = pictureButton.currentImage {
            Utils.shared.addPlayer(img, num: playerNum, odds: self.odds)
            Utils.loadPlayersFromMemory()
            self.backPressed()
        }
    }
    
    @objc func backPressed() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func cameraPressed() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            let picker = UIImagePickerController()
            picker.delegate = self
            picker.sourceType = .camera
            self.present(picker, animated: true, completion: nil)
        } else {
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary) {
                let picker = UIImagePickerController()
                picker.delegate = self
                picker.sourceType = .photoLibrary
                self.present(picker, animated: true, completion: nil)
            }
        }
    }
    
    
    // MARK: - TableView Delegate
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return PlayerEditCell.cellHeight
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Utils.shared.allPrizes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let prizeImage = Utils.shared.allPrizes[indexPath.row]
        var odd = 0
        if indexPath.row < odds.count {
            odd = odds[indexPath.row]
        }
        
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "cellName2") as! PlayerEditCell
        cell.setupWith(image: prizeImage, odds: odd, prizeNum: indexPath.row)
        print("odd: \(odd)")
        cell.selectionStyle = .none
        cell.delegate = self
        
        return cell
    }
    
    
    // MARK: - Star Delegate
    
    func starPressed(starCount: Int, prizeNum: Int) {
        odds[prizeNum] = starCount
        print(odds)
    }
    
    
    // MARK: - Image Picker Control Delegate
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            
            self.dismiss(animated: true, completion: {
                
                // Cropping
                let imageCropVC = RSKImageCropViewController(image: pickedImage, cropMode: .circle)
                imageCropVC.avoidEmptySpaceAroundImage = true
                imageCropVC.delegate = self
                imageCropVC.modalPresentationStyle = .fullScreen
                self.present(imageCropVC, animated: true, completion: nil)
            })
            
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    
    // MARK: - RSKImage Cropper
    
    func imageCropViewControllerDidCancelCrop(_ controller: RSKImageCropViewController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func imageCropViewController(_ controller: RSKImageCropViewController, didCropImage croppedImage: UIImage, usingCropRect cropRect: CGRect, rotationAngle: CGFloat) {
        
        let imagePicked = croppedImage
        let imageViewer = self.pictureButton
        let borderWidth: CGFloat = 2.0
        let cornerRadius:CGFloat = UIImageView(image: imagePicked).frame.size.width/2.0
        
        // Create a multiplier to scale up the corner radius and border
        // width you decided on relative to the imageViewer frame such
        // that the corner radius and border width can be converted to
        // the UIImage's scale.
        let multiplier:CGFloat = imagePicked.size.height/imageViewer.frame.size.height > imagePicked.size.width/imageViewer.frame.size.width ?
            imagePicked.size.height/imageViewer.frame.size.height :
            imagePicked.size.width/imageViewer.frame.size.width
        
        let borderWidthMultiplied:CGFloat = borderWidth * multiplier
        let cornerRadiusMultiplied:CGFloat = cornerRadius * multiplier
        
        UIGraphicsBeginImageContextWithOptions(imagePicked.size, false, 0)
        
//        let path = UIBezierPath(roundedRect: CGRectInset(CGRectMake(0, 0, imagePicked.size.width, imagePicked.size.height),
//                                                         borderWidthMultiplied / 2, borderWidthMultiplied / 2), cornerRadius: cornerRadiusMultiplied)
        let rec = CGRect(x: 0, y: 0, width: imagePicked.size.width, height: imagePicked.size.height).insetBy(dx: borderWidthMultiplied/2, dy: borderWidthMultiplied/2)
        let path = UIBezierPath(roundedRect: rec,
                                cornerRadius: cornerRadiusMultiplied)
        guard let context = UIGraphicsGetCurrentContext() else {
            self.dismiss(animated: true, completion: nil)
            return
        }
        
        context.saveGState()
        // Clip the drawing area to the path
        path.addClip()
        
        // Draw the image into the context
        imagePicked.draw(in: CGRect(x: 0, y: 0, width: imagePicked.size.width, height: imagePicked.size.height))
        context.restoreGState()
        
        // Configure the stroke
        UIColor.flatGreen().setStroke()
        path.lineWidth = borderWidthMultiplied
        
        // Stroke the border
        path.stroke()
        
        var imageFixed = UIGraphicsGetImageFromCurrentImageContext();
        
        UIGraphicsEndImageContext();
        
        imageFixed = imageFixed?.resizeWithWidth(width: 100)
        
        
        self.pictureButton.setImage(imageFixed, for: .normal)
        self.pictureButton.setTitle("", for: .normal)
        self.saveButton.isEnabled = true
        
        self.dismiss(animated: true, completion: nil)
    }
    
}


extension UIImage {
    func resizeWithPercent(percentage: CGFloat) -> UIImage? {
        let imageView = UIImageView(frame: CGRect(origin: .zero, size: CGSize(width: size.width * percentage, height: size.height * percentage)))
        imageView.contentMode = .scaleAspectFit
        imageView.image = self
        UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, false, scale)
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        imageView.layer.render(in: context)
        guard let result = UIGraphicsGetImageFromCurrentImageContext() else { return nil }
        UIGraphicsEndImageContext()
        return result
    }
    func resizeWithWidth(width: CGFloat) -> UIImage? {
        let imageView = UIImageView(frame: CGRect(origin: .zero, size: CGSize(width: width, height: CGFloat(ceil(width/size.width * size.height)))))
        imageView.contentMode = .scaleAspectFit
        imageView.image = self
        UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, false, scale)
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        imageView.layer.render(in: context)
        guard let result = UIGraphicsGetImageFromCurrentImageContext() else { return nil }
        UIGraphicsEndImageContext()
        return result
    }
}
