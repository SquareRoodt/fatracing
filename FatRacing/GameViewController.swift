//
//  ViewController.swift
//  FatRacing
//
//  Created by Jan-Dawid on 18/2/19.
//  Copyright © 2019 Jan-Dawid. All rights reserved.
//

import UIKit
import SpriteKit

class GameViewController: UIViewController {
    
    let yCount = 11
    let xCount = 11
    var winPoint = [10, 5]
    let blocks = [2,0,1,3] // 4 one blocks, 3 two blocks, 2 three blocks 1 four block
    let moveSpeed: TimeInterval = 0.5
    let start1 = 3
    let start2 = 5
    let start3 = 7
    var squares: [[Bool]] = [[]]  // ([y][x])
    var squareW: CGFloat = 1
    var squareH: CGFloat = 1
    let pictureWMultiplier: CGFloat = 0.25
    
    let resetButton = UIButton()
    let startButton = UIButton()
    var gameIsOver = false
    
    let mapView = UIView()
    var players: [Player] = [Player]()
    var spriteView: SKView?
    var scene: SKScene?
    var blockViews = [UIView]()
    var squareViews = [UIView]()
    
    let gridColour = UIColor.white
    let blockColour = UIColor.flatBlack()
    let squareColour = UIColor.flatGreen()
    
    
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadDataFromMemory()
        setupView()
    }
    
    func loadDataFromMemory() {
        players = Utils.shared.selectedPlayers
    }
    
    func setupView() {
        self.view.backgroundColor = .white
        
        // Back Button
        let backButton = UIButton()
        backButton.setBackgroundImage(UIImage(named: "BackButton"), for: .normal)
        backButton.addTarget(self, action: #selector(backPressed), for: .touchUpInside)
        self.view.addSubview(backButton)
        backButton.snp.makeConstraints { (make) -> Void in
            make.top.left.equalToSuperview().offset(20)
            make.width.height.equalTo(60)
        }
        
        // Reset Button
        resetButton.setBackgroundImage(UIImage(named: "ResetButton"), for: .normal)
        resetButton.addTarget(self, action: #selector(resetPressed), for: .touchUpInside)
        self.view.addSubview(resetButton)
        resetButton.snp.makeConstraints { (make) -> Void in
            make.top.width.height.equalTo(backButton)
            make.left.equalTo(backButton.snp.right).offset(10)
        }
        
        // Start Button
        startButton.backgroundColor = UIColor.flatGreen()
        startButton.setTitleColor(UIColor.white, for: .normal)
        startButton.setTitle("Start", for: .normal)
        startButton.titleLabel?.font = UIFont.systemFont(ofSize: 18, weight: .semibold)
        startButton.addTarget(self, action: #selector(startButtonPressed), for: .touchUpInside)
        startButton.layer.cornerRadius = 30
        startButton.clipsToBounds = true
        self.view.addSubview(startButton)
        startButton.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(resetButton)
            make.centerX.equalTo(self.view)
            make.width.equalTo(100)
            make.height.equalTo(60)
        }
        
        
        // Map View
        mapView.backgroundColor = gridColour
        self.view.addSubview(mapView)
        self.view.setNeedsLayout()
        mapView.snp.makeConstraints { (make) -> Void in
            make.left.right.bottom.equalToSuperview().inset(20)
            make.top.equalTo(resetButton.snp.bottom).offset(20)
        }
        self.view.layoutIfNeeded()
        
        // Prize Image
        let m = mapView.frame
        squareW = (m.width - (m.width * pictureWMultiplier)) / CGFloat(xCount)
        squareH = m.height / CGFloat(yCount)
        let prizeImageView = UIImageView()
        if Utils.shared.selectedPrize < Utils.shared.allPrizes.count {
            prizeImageView.image = Utils.shared.allPrizes[Utils.shared.selectedPrize]
        }
        let off: CGFloat = 20
        prizeImageView.frame = CGRect(x: (squareW * CGFloat(xCount)) + off,
                                      y: (m.height - (m.width * pictureWMultiplier))/2.0,
                                      width: m.width * pictureWMultiplier - off/2.0,
                                      height: m.width * pictureWMultiplier)
        mapView.addSubview(prizeImageView)
        
        // Grid Calculations
        
        
        // Sprite View
        let spriteFrame = CGRect(x: mapView.frame.origin.x,
                                 y: mapView.frame.origin.y,
                                 width: mapView.frame.width,
                                 height: mapView.frame.height)
        spriteView = SKView(frame: spriteFrame)
        spriteView?.backgroundColor = .clear
        self.view.addSubview(spriteView!)
        
        scene = SKScene(size: spriteView!.bounds.size)
        scene?.scaleMode = .aspectFit
        spriteView?.allowsTransparency = true
        scene?.backgroundColor = .clear
        spriteView?.allowsTransparency = true
        
        
        for (i, playerMem) in players.enumerated() {
            var player: Player
            
            let img = Utils.shared.getPlayerImage(playerMem.playerID)
            player = Player(texture: SKTexture(image: img))
            player.set(img: playerMem.image, stars: playerMem.goodOdds, ID: playerMem.playerID)
            
            guard let emitter = SKEmitterNode(fileNamed: "BasicParticle\(i+1).sks") else {
                print("ERROR: Failed to create SKEmitterNode")
                return
            }
            
            
            let playerWidth = squareH-5.0
            let borderW: CGFloat = 8
            let outlineFrame = CGRect(x: -(playerWidth)/2.0, y: -(playerWidth)/2.0, width: playerWidth, height: playerWidth)
            
            player.addChild(emitter)
            emitter.targetNode = scene
            if i == 0 {
                player.drawBorder(color: UIColor.flatBlue(), width: borderW, frame: outlineFrame)
                player.position = getPointFor(x: 0, y: start1)
                player.lastPoint = [0, start1]
                print(" - blue: \(player.goodOdds[Utils.shared.selectedPrize])")
//                print("blue: \(player.goodOdds)")
//                print("blue: \(player.playerID)")
            } else if i == 1 {
                player.drawBorder(color: UIColor.flatYellow(), width: borderW, frame: outlineFrame)
                player.position = getPointFor(x: 0, y: start2)
                player.lastPoint = [0, start2]
                print(" - yellow: \(player.goodOdds[Utils.shared.selectedPrize])")
//                print("yellow: \(player.goodOdds)")
//                print("yellow: \(player.playerID)")
            } else {
                player.drawBorder(color: UIColor.flatRed(), width: borderW, frame: outlineFrame)
                player.position = getPointFor(x: 0, y: start3)
                player.lastPoint = [0, start3]
                print(" - red: \(player.goodOdds[Utils.shared.selectedPrize])")
//                print("red: \(player.goodOdds)")
//                print("red: \(player.playerID)")
            }
            scene?.addChild(player)
            player.zPosition = 22
            player.size = CGSize(width: playerWidth, height: playerWidth)
            
            players.append(player)
        }
        
        spriteView?.presentScene(scene!)
        
        setupMap()
        
        for (i, yLine) in squares.enumerated() {
            for (j, _) in yLine.enumerated() {
                let v = UIView()
                v.backgroundColor = squareColour
                let p = getPointFor(x: j, y: i)
                v.frame = CGRect(x: p.x - squareW/2.0 + 0.25,
                                 y: p.y - squareH/2.0 + 0.25,
                                 width: squareW - 0.5,
                                 height: squareH - 0.5)
                self.mapView.addSubview(v)
            }
        }
        
        
        // Outlines
        for i in 0...7 {
            let line = UIView()
            line.backgroundColor = blockColour
            self.view.addSubview(line)
            let h: CGFloat = 5
            
            let mx = mapView.frame.origin.x
            let my = mapView.frame.origin.y
            let mw = (CGFloat(xCount) * squareW)
            let mh = mapView.frame.height
            let h2 = h + h
            let out:CGFloat = 20.0
            let outTop = my + getPointFor(x: winPoint[0], y: winPoint[1]).y - squareH/2.0
            let outBot = my + getPointFor(x: winPoint[0], y: winPoint[1]).y + squareH/2.0
            
            switch i {
            case 0:
                line.frame = CGRect(x: mx-h, y: my-h, width: mw+h2, height: h)
            case 1:
                line.frame = CGRect(x: mx-h, y: my-h, width: h, height: mh+h2)
            case 2:
                line.frame = CGRect(x: mx-h, y: my+mh, width: mw+h2, height: h)
            case 3:
                line.frame = CGRect(x: mx+mw,
                                    y: outBot,
                                    width: h,
                                    height: outBot - my - squareH)
            case 4:
                line.frame = CGRect(x: mx+mw,
                                    y: my,
                                    width: h,
                                    height: outTop - my)
            case 5:
                line.frame = CGRect(x: mx+mw,
                                    y: outTop,
                                    width: out,
                                    height: h)
            case 6:
                line.frame = CGRect(x: mx+mw,
                                    y: outBot,
                                    width: out,
                                    height: h)
            default:
                break
            }
        }
        
        setupMap()
    }
    
    @objc func setupMap() {
        
        squares = Array(repeating: Array(repeating: true, count: xCount), count: yCount)
        
        // Blocks
        for b in blockViews {
            b.removeFromSuperview()
        }
        blockViews.removeAll()
        
        // blocks
        for (i, blockCount) in blocks.enumerated() {
            for _ in 0..<blockCount {
                makeRandomBlocks(i+1)
            }
        }
        
        printSquares()
    }
    
    @objc func backPressed() {
        for player in players {
            player.removeAllActions()
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func resetPressed() {
        
        var times: [Double] = [0.0]
        
        for i in 1...15 {
            times.append(0.05 * Double(i))
        }
        for i in 1...15 {
            times.append((15*0.05) + (0.1 * Double(i)))
        }
        
        let l = times.last!
        times.append(contentsOf: [l+0.15, l+0.3, l+0.5, l+0.8, l+1.4])
        
        
        for time in times {
            DispatchQueue.main.asyncAfter(deadline: .now() + time) {
                self.setupMap()
            }
        }
        
    }
    
    @objc func startButtonPressed() {
        startButton.isEnabled = false
        resetButton.isEnabled = false
        
        print("prize \(Utils.shared.selectedPrize)")
        
        for (i, player) in players.enumerated() {
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + (0.1 * Double(i))) {
                player.lastPoint = [player.lastPoint[0], player.lastPoint[1]]
                let current = [player.lastPoint[0] + 1, player.lastPoint[1]]
                let act = SKAction.move(to: self.getPointFor(x: current[0], y: current[1]), duration: self.moveSpeed)
                
                player.run(act) {
                    self.takeTurnFor(player: player, last: player.lastPoint, current: current)
                }
            }
        }
    }
    
    func takeTurnFor(player: Player, last: [Int], current: [Int]) {
        
        if current[0] == winPoint[0] && current[1] == winPoint[1] {
            
            let p = getPointFor(x: winPoint[0], y: winPoint[1])
            let act = SKAction.moveBy(x: squareW * 3, y: 0, duration: moveSpeed)
            player.run(act) {
                self.gameOverWithWinner(player.playerID)
            }
            return
        }
        
        let odds = Utils.shared.selectedPrize > player.goodOdds.count ? Player.star1Odds : player.goodOdds[Utils.shared.selectedPrize]
        let new = getNextPointFrom(last: last, current: current, goodOdds: odds)
        let p = self.getPointFor(x: new[0], y: new[1])
        
        let act = SKAction.move(to: p, duration: self.moveSpeed)
        player.run(act) {
            // if win return
            player.removeAllActions()
            self.takeTurnFor(player: player, last: current, current: new)
        }
    }
    
    func getNextPointFrom(last: [Int], current: [Int], goodOdds: Int) -> [Int] {
        var good = [[Int]]()
        var bad = [[Int]]()
        let x = current[0]
        let y = current[1]
        
        if x-1 >= 0 {
            if squares[y][x-1] && x-1 != last[0] {
                if abs(x-1-winPoint[0]) < abs(x-winPoint[0]) {
                    good.append([x-1, y])
                } else {
                    bad.append([x-1, y])
                }
            }
        }
        if x+1 <= xCount-1 {
            if squares[y][x+1] && x+1 != last[0] {
                if abs(x+1-winPoint[0]) < abs(x-winPoint[0]) {
                    good.append([x+1, y])
                } else {
                    bad.append([x+1, y])
                }
            }
        }
        if y-1 >= 0 {
            if squares[y-1][x] && y-1 != last[1] {
                if abs(y-1-winPoint[1]) < abs(y-winPoint[1]) {
                    good.append([x, y-1])
                } else {
                    bad.append([x, y-1])
                }
            }
        }
        if y+1 <= xCount-1 {
            if squares[y+1][x] && y+1 != last[1] {
                if abs(y+1-winPoint[1]) < abs(y-winPoint[1]) {
                    good.append([x, y+1])
                } else {
                    bad.append([x, y+1])
                }
            }
        }
        
//        print("good: \(good)")
//        print("bad: \(bad)")
        
        // turn around
        if good.count == 0 && bad.count == 0 {
//            print("turn around - \(last)")
            return last
        }
        // no good options
        if good.count == 0 {
            let r = random(low: 0, high: bad.count-1)
//            print("no good - \(bad[r])")
            return bad[r]
        }
        // no bad options
        if bad.count == 0 {
            let r = random(low: 0, high: good.count-1)
//            print("no bad - \(good[r])")
            return good[r]
        }
        // random good/bad
        let randomNum = random(low: 0, high: 100)
        if (0...goodOdds).contains(randomNum) && good.count != 0 {
            let r = random(low: 0, high: good.count-1)
//            print("random good - \(good[r])")
            return good[r]
        } else {
            let r = random(low: 0, high: bad.count-1)
//            print("random bad - \(bad[r])")
            return bad[r]
        }
    }
    
    func gameOverWithWinner(_ winningPlayerNum: Int) {
        
        if gameIsOver {
            return
        }
        gameIsOver = true
        
        let vc = WinnerViewController()
        vc.setWinningNum(winningPlayerNum)
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    
    // MARK: - Grid Setup
    
    func makeRandomBlocks(_ amount: Int) {
        let startX = random(low: 2, high: xCount-2)
        let startY = random(low: 0, high: yCount-1)
        makeBlocks(x: startX, y: startY, total: amount)
    }
    
    func makeBlocks(x: Int, y: Int, total: Int) {
        squares[y][x] = false
        let block = UIView()
        block.backgroundColor = blockColour
        let p = getPointFor(x: x, y: yCount - 1 - y)
        block.frame = CGRect(x: p.x-squareW/2.0,
                               y: p.y-squareH/2.0,
                               width: squareW,
                               height: squareH)
        self.mapView.addSubview(block)
        blockViews.append(block)
        
        
        
        if total-1 <= 0 {
            return
        }
        
        var possible = [[Int]]()
        
        if x-1 >= 2 {
            if squares[y][x-1] {
                possible.append([x-1, y])
            }
        }
        if x+1 <= xCount-2 {
            if squares[y][x+1] {
                possible.append([x+1, y])
            }
        }
        if y-1 >= 1 {
            if squares[y-1][x] {
                possible.append([x, y-1])
            }
        }
        if y+1 <= xCount-1 {
            if squares[y+1][x] {
                possible.append([x, y+1])
            }
        }
        
        if possible.count == 0 {
            return
        }
        
        let next = random(low: 0, high: possible.count-1)
        makeBlocks(x: possible[next][0], y: possible[next][1], total: total-1)
    }
    
    
    func printSquares() {
        for yLine in squares {
            var lineString = ""
            for xBlock in yLine {
                lineString += xBlock ? "[ ]" : "[X]"
            }
//            print(lineString)
        }
//        print()
    }
    
    // inclusive
    func random(low: Int, high: Int) -> Int {
        let l = UInt32(low)
        let h = UInt32(high+1)
        return Int(arc4random_uniform(h-l) + l)
    }
    
    func getPointFor(x: Int, y: Int) -> CGPoint {
//        let xx = (squareW * CGFloat(xCount-1))
        let yy = (squareH * CGFloat(yCount-1))
        return CGPoint(x: (squareW * CGFloat(x)) + (squareW/2.0),
                       y: yy - (squareH * CGFloat(y)) + (squareH/2.0))
    }
}


