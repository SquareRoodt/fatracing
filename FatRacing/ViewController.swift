//
//  ViewController.swift
//  FatRacing
//
//  Created by Jan-Dawid Roodt on 18/02/19.
//  Copyright © 2019 JD. All rights reserved.
//

import UIKit
import ChameleonFramework
import SnapKit
import Firebase

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    let player1 = UIImageView()
    let player2 = UIImageView()
    let player3 = UIImageView()
    let tableView = UITableView()
    let randomPrizeButton = UIButton()
    var players = [Player]()
    
    var ref: DatabaseReference!
    var allowBets: Bool = true
    
    
    // MAIN LOADING METHOD. INSERT STUFF HERE
    func loadPlayersAndPrizes() {
        // Adding Prizes
        if Utils.shared.allPrizes.count == 0 {
            var continueChecking = true
            var i = 1
            while continueChecking {
                if let prizeImage = UIImage(named: "chocolate\(i)") {
                    i += 1
                    Utils.savePrize(prizeImage)
                } else {
                    continueChecking = false
                }
            }
        }
        
        // Adding Players
        if Utils.shared.allPlayers.count == 0 {
            var i = 1
            Utils.shared.addPlayer(UIImage(named: "player\(i)")!.circularImage()!, num: i-1, odds: [3, 2, 3, 1, 1, 1, 2, 3, 3, 3, 3, 1, 1])
            i += 1
            Utils.shared.addPlayer(UIImage(named: "player\(i)")!.circularImage()!, num: i-1, odds: [3, 2, 3, 1, 2, 1, 1, 3, 1, 2, 3, 3, 2])
            i += 1
            Utils.shared.addPlayer(UIImage(named: "player\(i)")!.circularImage()!, num: i-1, odds: [2, 2, 1, 3, 1, 3, 2, 2, 3, 3, 2, 1, 2])
            i += 1
            Utils.shared.addPlayer(UIImage(named: "player\(i)")!.circularImage()!, num: i-1, odds: [2, 2, 2, 2, 3, 3, 1, 3, 3, 3, 2, 2, 3])
            i += 1
            Utils.shared.addPlayer(UIImage(named: "player\(i)")!.circularImage()!, num: i-1, odds: [1, 3, 2, 1, 1, 2, 1, 3, 3, 2, 1, 1, 2])
            i += 1
            Utils.shared.addPlayer(UIImage(named: "player\(i)")!.circularImage()!, num: i-1, odds: [2, 3, 3, 1, 2, 1, 1, 3, 3, 3, 3, 2, 1])
            i += 1
            Utils.shared.addPlayer(UIImage(named: "player\(i)")!.circularImage()!, num: i-1, odds: [2, 2, 1, 3, 3, 2, 3, 3, 3, 3, 2, 1, 3])
            i += 1
            Utils.shared.addPlayer(UIImage(named: "player\(i)")!.circularImage()!, num: i-1, odds: [2, 3, 3, 1, 1, 1, 3, 2, 3, 2, 1, 1, 1])
            i += 1
            Utils.shared.addPlayer(UIImage(named: "player\(i)")!.circularImage()!, num: i-1, odds: [3, 3, 2, 2, 2, 3, 1, 3, 2, 3, 2, 2, 3])
            i += 1
            Utils.shared.addPlayer(UIImage(named: "player\(i)")!.circularImage()!, num: i-1, odds: [3, 1, 3, 3, 2, 1, 3, 3, 2, 2, 3, 3, 1])
            i += 1
            Utils.shared.addPlayer(UIImage(named: "player\(i)")!.circularImage()!, num: i-1, odds: [1, 1, 2, 1, 1, 1, 2, 1, 3, 3, 2, 1, 1])
            i += 1
            Utils.shared.addPlayer(UIImage(named: "player\(i)")!.circularImage()!, num: i-1, odds: [1, 1, 3, 1, 1, 1, 1, 3, 1, 3, 3, 1, 3])
            i += 1
            Utils.shared.addPlayer(UIImage(named: "player\(i)")!.circularImage()!, num: i-1, odds: [2, 3, 2, 1, 3, 2, 3, 3, 3, 3, 3, 2, 3])
            i += 1
            Utils.shared.addPlayer(UIImage(named: "player\(i)")!.circularImage()!, num: i-1, odds: [2, 2, 3, 1, 2, 2, 1, 2, 2, 3, 2, 3, 2])
            i += 1
            Utils.shared.addPlayer(UIImage(named: "player\(i)")!.circularImage()!, num: i-1, odds: [3, 3, 2, 3, 3, 2, 1, 2, 3, 1, 2, 2, 3])
            i += 1
            Utils.shared.addPlayer(UIImage(named: "player\(i)")!.circularImage()!, num: i-1, odds: [3, 3, 3, 2, 1, 2, 3, 3, 3, 2, 1, 2, 1])
            i += 1
            Utils.shared.addPlayer(UIImage(named: "player\(i)")!.circularImage()!, num: i-1, odds: [3, 2, 3, 3, 2, 2, 2, 1, 3, 1, 1, 2, 3])
            i += 1
            Utils.shared.addPlayer(UIImage(named: "player\(i)")!.circularImage()!, num: i-1, odds: [2, 3, 2, 3, 2, 3, 2, 3, 3, 3, 3, 2, 3])
            i += 1
            Utils.shared.addPlayer(UIImage(named: "player\(i)")!.circularImage()!, num: i-1, odds: [3, 3, 1, 3, 2, 3, 2, 3, 3, 3, 1, 1, 3])
            i += 1
            Utils.shared.addPlayer(UIImage(named: "player\(i)")!.circularImage()!, num: i-1, odds: [3, 3, 3, 3, 2, 3, 1, 3, 3, 3, 2, 1, 3])
            i += 1
            Utils.shared.addPlayer(UIImage(named: "player\(i)")!.circularImage()!, num: i-1, odds: [1, 3, 3, 3, 3, 3, 3, 1, 3, 2, 3, 2, 1])
            i += 1
            Utils.shared.addPlayer(UIImage(named: "player\(i)")!.circularImage()!, num: i-1, odds: [1, 3, 2, 3, 1, 1, 1, 3, 2, 3, 2, 1, 1])
            i += 1
            Utils.shared.addPlayer(UIImage(named: "player\(i)")!.circularImage()!, num: i-1, odds: [3, 3, 3, 3, 3, 3, 2, 1, 1, 2, 3, 3, 3])
            i += 1
            
        }
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        Utils.loadPrizesFromMemory()
        Utils.loadPlayersFromMemory()
        setupView()
        randomPlayers()
//        randomPrizePressed()
        self.randomPrizeButton.setBackgroundImage(Utils.getRandomPrize(), for: .normal)
        
        ref = Database.database().reference()
        setupFirebase()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        Utils.loadPlayersFromMemory()  // can't have it there, it resets score
        sortAndreloadData()
        
        if Utils.shared.allPlayers.count == 0 {
            randomPlayers()
        }
        
        allowBets = true
    }
    
    func setupView() {
        self.view.backgroundColor = .white
        
        let playerH: CGFloat = PlayerCell.suggestedHeight - 5
        let toolW: CGFloat = Utils.isIpad()         ? Utils.screenWidth()/3.0 : 220
        let toolButtonW: CGFloat = Utils.isIpad()   ? 180 : 150
        let toolButtonH: CGFloat = Utils.isIpad()   ? 100 : 50
        
        // Tools View
        let toolView = UIView()
        self.view.addSubview(toolView)
        toolView.snp.makeConstraints { (make) -> Void in
            make.width.equalTo(toolW)
            make.bottom.right.equalToSuperview()
            make.top.equalToSuperview().offset(playerH+20)
        }
        
        // TableView
        tableView.register(PlayerCell.self, forCellReuseIdentifier: "cellName")
        tableView.delegate = self
        tableView.dataSource = self
        self.view.addSubview(tableView)
        tableView.snp.makeConstraints { (make) -> Void in
            make.left.bottom.equalToSuperview()
            make.right.equalTo(toolView.snp.left)
            make.top.equalTo(20 + playerH)
        }
        
        // Player Icons
        let players = [player3, player2, player1]
        var playerOffset: CGFloat = (5 + playerH) * 2
        for (i, player) in players.enumerated() {
            
            player.clipsToBounds = true
            player.layer.borderWidth = 10
            player.layer.cornerRadius = playerH/2.0
            if Utils.shared.selectedPlayers.count > i {
                player.image = Utils.shared.selectedPlayers[i].image
            }
            self.view.addSubview(player)
            player.snp.makeConstraints { (make) -> Void in
                make.top.equalToSuperview().offset(20)
                make.right.equalTo(self.tableView.snp.right).inset(playerOffset)
                make.width.height.equalTo(playerH)
            }
            playerOffset -= (playerH + 5)
        }
        player3.backgroundColor = UIColor.flatRed()
        player3.layer.borderColor = UIColor.flatRed().cgColor
        player2.backgroundColor = UIColor.flatYellow()
        player2.layer.borderColor = UIColor.flatYellow().cgColor
        player1.backgroundColor = UIColor.flatBlue()
        player1.layer.borderColor = UIColor.flatBlue().cgColor
        
        // Settings Button
        let settingsButton = UIButton()
        settingsButton.setBackgroundImage(UIImage(named: "SettingsIcon"), for: .normal)
        settingsButton.addTarget(self, action: #selector(settingsPressed), for: .touchUpInside)
        self.view.addSubview(settingsButton)
        settingsButton.snp.makeConstraints { (make) -> Void in
            make.top.left.equalToSuperview().offset(20)
            make.width.height.equalTo(playerH-10)
        }

        // Start Button
        let startButton = UIButton()
        startButton.backgroundColor = UIColor.flatGreen()
        startButton.setTitleColor(UIColor.white, for: .normal)
        startButton.setTitle("Race!", for: .normal)
        startButton.titleLabel?.font = UIFont.systemFont(ofSize: 25, weight: .semibold)
        startButton.addTarget(self, action: #selector(startButtonPressed), for: .touchUpInside)
        startButton.layer.cornerRadius = toolButtonH/2.0
        startButton.clipsToBounds = true
        toolView.addSubview(startButton)
        startButton.snp.makeConstraints { (make) -> Void in
            make.centerX.equalToSuperview()
            make.bottom.equalToSuperview().offset(-50)
            make.width.equalTo(toolButtonW)
            make.height.equalTo(toolButtonH)
        }
        
        // Player Shuffle Button
        let randomPlayerButton = UIButton()
        randomPlayerButton.backgroundColor = UIColor.flatGreen()
        randomPlayerButton.setTitleColor(UIColor.white, for: .normal)
        randomPlayerButton.setTitle("Shuffle", for: .normal)
        randomPlayerButton.titleLabel?.font = UIFont.systemFont(ofSize: 25, weight: .semibold)
        randomPlayerButton.addTarget(self, action: #selector(randomPlayersPressed), for: .touchUpInside)
        randomPlayerButton.layer.cornerRadius = toolButtonH/2.0
        randomPlayerButton.clipsToBounds = true
        toolView.addSubview(randomPlayerButton)
        randomPlayerButton.snp.makeConstraints { (make) -> Void in
            make.centerX.equalToSuperview()
            make.bottom.equalTo(startButton.snp.top).offset(-20)
            make.width.equalTo(toolButtonW)
            make.height.equalTo(toolButtonH)
        }
        
        // Prize Button
        randomPrizeButton.layer.borderColor = UIColor.flatGreen().cgColor
        randomPrizeButton.layer.borderWidth = 10.0
        randomPrizeButton.layer.cornerRadius = 20.0
        randomPrizeButton.clipsToBounds = true
        randomPrizeButton.addTarget(self, action: #selector(randomPrizePressed), for: .touchUpInside)
        toolView.addSubview(randomPrizeButton)
        let prizeW: CGFloat = Utils.isIpad() ? toolButtonW : toolButtonW - 10
        randomPrizeButton.snp.makeConstraints { (make) -> Void in
                make.centerX.equalToSuperview()
                make.bottom.equalTo(randomPlayerButton.snp.top).offset(-20)
                make.width.equalTo(prizeW)
                make.height.equalTo(prizeW)
        }
        
        loadPlayersAndPrizes()
        
        sortAndreloadData()
    }
    
    func setupFirebase() {
        ref.observe(.value) { snapshot in
            
            if !self.allowBets {
                return
            }
            
            if let value = snapshot.value as? NSDictionary {
                for i in 0...self.players.count {
                    
                    if let packedPlayer = value[Utils.getPlayerNameForID(i)] as? NSDictionary{
                        if let amountString = packedPlayer["bet"] {
                            var betPersonFinal: Int? = nil
                            var betAmountFinal: Int? = nil
                            
                            if let text = amountString as? String, text.count >= 2 {
                                if let betPerson = text.first {
                                    if betPerson.uppercased() == "B" {
                                        betPersonFinal = 1
                                    } else if betPerson.uppercased() == "Y" {
                                        betPersonFinal = 2
                                    } else if betPerson.uppercased() == "R" {
                                        betPersonFinal = 3
                                    }
                                }
                                
                                var moneyString = text
                                moneyString.remove(at: moneyString.startIndex)
                                
                                if let money = Int(moneyString) {
                                    betAmountFinal = money
                                }
                                
                                if let money = betAmountFinal, let playerID = betPersonFinal,
                                   self.players[i].currentTotalAmount >= money && money > 0 {
                                    Utils.betPlayerID(i, amount: money, onPlayerNum: playerID)
                                } else {
                                    print("Failed to update player(\(self.players[i].playerID))|\(Utils.getPlayerNameForID(self.players[i].playerID)):    bet amount: \(String(describing: betAmountFinal)),    toPlayerID: \(String(describing: betPersonFinal))")
                                }
                            } else {
        //                        print("player(\(self.players[i].playerID))")
                            }
                        }
                        
                    }
                }
                
                self.sortAndreloadData()
            }
        }
    }
    
    func sortAndreloadData() {
        players.removeAll()
        players = Utils.shared.allPlayers
        players.sort(by: { $0.currentTotalAmount > $1.currentTotalAmount })
        self.tableView.reloadData()
    }
    
    
    
    
    // MARK: - Actions

    @objc func startButtonPressed() {
        allowBets = false
        print("SETUP RACE with prize \(Utils.shared.selectedPrize)")
        let vc = GameViewController()
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    @objc func settingsPressed() {
        print("Settings pressed")
        let vc = SettingsViewController()
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    @objc func randomPrizePressed() {
        
        var times: [Double] = [0.0]
        for i in 1...15 {
            times.append(0.05 * Double(i))
        }
        for i in 1...10 {
            times.append((15*0.05) + (0.1 * Double(i)))
        }
        let l = times.last!
        times.append(contentsOf: [l+0.15, l+0.3, l+0.5, l+1.0, l+1.8])
        for time in times {
            DispatchQueue.main.asyncAfter(deadline: .now() + time) {
                self.randomPrizeButton.setBackgroundImage(Utils.getRandomPrize(), for: .normal)
            }
        }
    }
    
    @objc func randomPlayersPressed() {
        var times: [Double] = [0.0]
        for i in 1...15 {
            times.append(0.05 * Double(i))
        }
        for i in 1...10 {
            times.append((15*0.05) + (0.1 * Double(i)))
        }
        let l = times.last!
        times.append(contentsOf: [l+0.15, l+0.3, l+0.5, l+1.0, l+1.8])
        for time in times {
            DispatchQueue.main.asyncAfter(deadline: .now() + time) {
                self.randomPlayers()
            }
        }
    }
    
    @objc func randomPlayers() {
        let topPlayerImages = [player1, player2, player3]
        for p in topPlayerImages {
            p.image = UIImage()
        }
        
        Utils.shared.selectedPlayers.removeAll()
        let userCount = Utils.shared.allPlayers.count-1
        var selectedNums = [Int]()
        if userCount <= 0 {return}
        for i in 0...userCount {
            if i >= 3 {
                break
            }
            let r = Utils.random(low: 0, high: userCount)
            if !selectedNums.contains(r) {
                selectedNums.append(r)
                Utils.shared.selectedPlayers.append(Utils.shared.allPlayers[r])
            } else {
                var j = r
                while j <= userCount && selectedNums.contains(j) {
                    if j+1 > userCount {
                        j = 0
                    } else {
                        j += 1
                    }
                }
                selectedNums.append(j)
                Utils.shared.selectedPlayers.append(Utils.shared.allPlayers[j])
            }
            
            topPlayerImages[i].image = Utils.shared.selectedPlayers[i].image
        }
        print(selectedNums)
        sortAndreloadData()
    }
    
    
    // MARK: - Table View Delegates
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return players.count
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "cellName") as! PlayerCell
            cell.setupWithPlayer(players[indexPath.row])
            cell.selectionStyle = .none
            return cell
        } else {
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "cellName") as! PlayerCell
            cell.setupAsAddition()
            cell.selectionStyle = .none
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return PlayerCell.suggestedHeight
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = NewPlayerViewController()
        if indexPath.section == 1 {
            // Don't load user
        } else if indexPath.section == 0 {
            vc.loadPlayer(players[indexPath.row])
        }
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }

}

