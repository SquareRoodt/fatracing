//
//  PlayerEditCell.swift
//  FatRacing
//
//  Created by Jan-Dawid on 25/2/19.
//  Copyright © 2019 JD. All rights reserved.
//

import UIKit

protocol PlayerEditCellDelegate:class {
    func starPressed(starCount: Int, prizeNum: Int)
}


class PlayerEditCell: UITableViewCell {
    
    static let cellHeight: CGFloat = Utils.screenWidth() > 400 ? 120 : 70
    static let cellMaxWidth: CGFloat = Utils.screenWidth() < 800 ? Utils.screenWidth() : 800
    let prizeImage = UIImageView()
    let star1 = UIButton()
    let star2 = UIButton()
    let star3 = UIButton()
    var prizeNum: Int = 0
    
    weak var delegate: PlayerEditCellDelegate?
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupBaseView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setupBaseView() {
        // Prize
        self.contentView.addSubview(prizeImage)
        prizeImage.frame = CGRect(x: 0, y: 0, width: PlayerEditCell.cellHeight, height: PlayerEditCell.cellHeight)
        
        // Star Buttons
        var currentX = PlayerEditCell.cellMaxWidth - PlayerEditCell.cellHeight
        
        star3.setImage(UIImage(named: "StarEmpty"), for: .normal)
        star3.setImage(UIImage(named: "StarFull"), for: .selected)
        star3.frame = CGRect(x: currentX, y: 0, width: PlayerEditCell.cellHeight, height: PlayerEditCell.cellHeight)
        self.contentView.addSubview(star3)
        currentX -= PlayerEditCell.cellHeight
        
        star2.setImage(UIImage(named: "StarEmpty"), for: .normal)
        star2.setImage(UIImage(named: "StarFull"), for: .selected)
        star2.frame = CGRect(x: currentX, y: 0, width: PlayerEditCell.cellHeight, height: PlayerEditCell.cellHeight)
        self.contentView.addSubview(star2)
        currentX -= PlayerEditCell.cellHeight
        
        star1.setImage(UIImage(named: "StarEmpty"), for: .normal)
        star1.setImage(UIImage(named: "StarFull"), for: .selected)
        star1.frame = CGRect(x: currentX, y: 0, width: PlayerEditCell.cellHeight, height: PlayerEditCell.cellHeight)
        self.contentView.addSubview(star1)
        
    }
    
    func setupWith(image: UIImage, odds: Int, prizeNum: Int) {
        prizeImage.image = image
        self.prizeNum = prizeNum
        let stars = [star1, star2, star3]
        for star in stars {
            star.isSelected = false
            star.removeTarget(nil, action: nil, for: .allEvents)
            star.addTarget(self, action: #selector(activateStar(_:)), for: .touchUpInside)
        }
        if !(1...3).contains(odds) {return}
        var i = 0
        while i < odds {
            stars[i].isSelected = true
            i += 1
        }
    }
    
    @objc func activateStar(_ sender: UIButton) {
        let stars = [star1, star2, star3]
        for star in stars {
            star.isSelected = false
        }
        
        if sender == star1 {
            star1.isSelected = true
            delegate?.starPressed(starCount: 1, prizeNum: self.prizeNum)
        } else if sender == star2 {
            star1.isSelected = true
            star2.isSelected = true
            delegate?.starPressed(starCount: 2, prizeNum: self.prizeNum)
        } else if sender == star3 {
            star1.isSelected = true
            star2.isSelected = true
            star3.isSelected = true
            delegate?.starPressed(starCount: 3, prizeNum: self.prizeNum)
        }
    }
    
}
