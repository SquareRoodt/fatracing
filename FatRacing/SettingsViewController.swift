//
//  SettingsViewController.swift
//  FatRacing
//
//  Created by Jan-Dawid on 12/3/19.
//  Copyright © 2019 JD. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    func setupView() {
        self.view.backgroundColor = .white
        
        // Back Button
        let backButton = UIButton()
        backButton.setBackgroundImage(UIImage(named: "BackButton"), for: .normal)
        backButton.addTarget(self, action: #selector(backPressed), for: .touchUpInside)
        self.view.addSubview(backButton)
        backButton.snp.makeConstraints { (make) -> Void in
            make.top.left.equalToSuperview().offset(20)
            make.width.height.equalTo(60)
        }
        
        let toolButtonW: CGFloat = Utils.isIpad() ? 180 : 150
        let toolButtonH: CGFloat = Utils.isIpad() ? 150 : 100
        
        // Delete Players
        let deletePlayersButton = UIButton()
        deletePlayersButton.backgroundColor = UIColor.flatGreen()
        deletePlayersButton.setTitleColor(UIColor.white, for: .normal)
        deletePlayersButton.setTitle("Delete\nPlayers", for: .normal)
        deletePlayersButton.titleLabel?.font = UIFont.systemFont(ofSize: 22, weight: .semibold)
        deletePlayersButton.titleLabel?.numberOfLines = 2
        deletePlayersButton.addTarget(self, action: #selector(deleteAllPlayers), for: .touchUpInside)
        deletePlayersButton.layer.cornerRadius = toolButtonH/4.0
        deletePlayersButton.clipsToBounds = true
        self.view.addSubview(deletePlayersButton)
        deletePlayersButton.snp.makeConstraints { (make) -> Void in
            make.left.equalToSuperview().offset(20)
            make.top.equalTo(backButton.snp.bottom).offset(20)
            make.width.equalTo(toolButtonW)
            make.height.equalTo(toolButtonH)
        }
        
        // Reset Players
        let resetPlayersButton = UIButton()
        resetPlayersButton.backgroundColor = UIColor.flatGreen()
        resetPlayersButton.setTitleColor(UIColor.white, for: .normal)
        resetPlayersButton.setTitle("Reset\nCoins", for: .normal)
        resetPlayersButton.titleLabel?.font = UIFont.systemFont(ofSize: 22, weight: .semibold)
        resetPlayersButton.titleLabel?.numberOfLines = 2
        resetPlayersButton.addTarget(self, action: #selector(resetCoins), for: .touchUpInside)
        resetPlayersButton.layer.cornerRadius = toolButtonH/4.0
        resetPlayersButton.clipsToBounds = true
        self.view.addSubview(resetPlayersButton)
        resetPlayersButton.snp.makeConstraints { (make) -> Void in
            make.left.equalToSuperview().offset(20)
            make.top.equalTo(deletePlayersButton.snp.bottom).offset(20)
            make.width.equalTo(toolButtonW)
            make.height.equalTo(toolButtonH)
        }
        
        // Reset Prizes
        let resetPrizesButton = UIButton()
        resetPrizesButton.backgroundColor = UIColor.flatGreen()
        resetPrizesButton.setTitleColor(UIColor.white, for: .normal)
        resetPrizesButton.setTitle("Re-add\nTargets", for: .normal)
        resetPrizesButton.titleLabel?.font = UIFont.systemFont(ofSize: 22, weight: .semibold)
        resetPrizesButton.titleLabel?.numberOfLines = 2
        resetPrizesButton.addTarget(self, action: #selector(deleteAllPrizes), for: .touchUpInside)
        resetPrizesButton.layer.cornerRadius = toolButtonH/4.0
        resetPrizesButton.clipsToBounds = true
        self.view.addSubview(resetPrizesButton)
        resetPrizesButton.snp.makeConstraints { (make) -> Void in
            make.left.equalToSuperview().offset(20)
            make.top.equalTo(resetPlayersButton.snp.bottom).offset(20)
            make.width.equalTo(toolButtonW)
            make.height.equalTo(toolButtonH)
        }
    }
    
    @objc func backPressed() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func deleteAllPlayers() {
        let alert = UIAlertController(title: "Delete All Players", message: "Are you sure you want to delete all players", preferredStyle: .alert)
        let deleteButton = UIAlertAction(title: "Delete", style: .destructive) { (action) in
            Utils.deleteAllPlayers()
        }
        let cancelButton = UIAlertAction(title: "Cancel", style: .cancel) { (action) in}
        alert.addAction(deleteButton)
        alert.addAction(cancelButton)
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func resetCoins() {
        let alert = UIAlertController(title: "Reset All Coins", message: "Are you sure you want to reset everyone's coin count to \(Utils.startingCoinAmount)", preferredStyle: .alert)
        let resetButton = UIAlertAction(title: "Reset", style: .destructive) { (action) in
            for player in Utils.shared.allPlayers {
                player.currentTotalAmount = Utils.startingCoinAmount
            }
        }
        let cancelButton = UIAlertAction(title: "Cancel", style: .cancel) { (action) in}
        alert.addAction(resetButton)
        alert.addAction(cancelButton)
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func deleteAllPrizes() {
        let alert = UIAlertController(title: "Delete All Targets", message: "Deleting Targets will also delete all the players", preferredStyle: .alert)
        let deleteButton = UIAlertAction(title: "Delete", style: .destructive) { (action) in
            Utils.deleteAllTargets()
            
        }
        let cancelButton = UIAlertAction(title: "Cancel", style: .cancel) { (action) in}
        alert.addAction(deleteButton)
        alert.addAction(cancelButton)
        self.present(alert, animated: true, completion: nil)
        
    }

}
