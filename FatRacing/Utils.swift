//
//  Utils.swift
//  FatRacing
//
//  Created by Jan-Dawid Roodt on 18/02/19.
//  Copyright © 2019 JD. All rights reserved.
//

import Foundation
import UIKit
import SpriteKit
import Firebase

@objc class Utils: NSObject {
    
    static let startingCoinAmount = 10
    static let winningMultiplier = 2
    static let shared = Utils()
    var selectedPrize: Int = 0
    var allPrizes = [UIImage]()
    var allPlayers = [Player]()
    var selectedPlayers = [Player]()
    
    static func screenWidth() -> CGFloat {
        return UIScreen.main.bounds.size.width
    }
    
    static func screenHeight() -> CGFloat {
        return UIScreen.main.bounds.size.height
    }
    
    static func isIpad() -> Bool {
        return UIDevice.current.userInterfaceIdiom == .pad
    }
    
    static func isIphone() -> Bool {
        return UIDevice.current.userInterfaceIdiom == .phone
    }
    
    static func getRandomPrize() -> UIImage? {
        if shared.allPrizes.count == 0 {
            return nil
        }
        let ran = random(low: 0, high: shared.allPrizes.count-1)
        shared.selectedPrize = ran
        return shared.allPrizes[ran]
    }
    
    static func loadPrizesFromMemory() {
        var moreToSearch = true
        var i = 0
        
        while moreToSearch {
            if let prize = shared.getImageFor(name: "prize\(i)") {
                shared.allPrizes.append(prize)
                i += 1
            } else {
                moreToSearch = false
            }
        }
    }
    
    static func savePrize(_ img: UIImage) {
        shared.saveImage(image: img, withName: "prize\(shared.allPrizes.count)")
        shared.allPrizes.append(img)
    }
    
    // inclusive
    static func random(low: Int, high: Int) -> Int {
        let l = UInt32(low)
        let h = UInt32(high+1)
        return Int(arc4random_uniform(h-l) + l)
    }
    
    func getPlayerImage(_ playerNum: Int) -> UIImage {
        return getImageFor(name: "player\(playerNum)") ?? UIImage()
    }
    
    static func loadPlayersFromMemory() {
        var moreToSearch = true
        var i = 0
        shared.allPlayers.removeAll()
        
        while moreToSearch {
            if let player = shared.getPlayer(num: i) {
                shared.allPlayers.append(player)
                i += 1
            } else {
                moreToSearch = false
            }
        }
    }
    
    static func betPlayerID(_ ID: Int, amount: Int, onPlayerNum: Int) {
        for (i, p) in Utils.shared.allPlayers.enumerated() {
            if p.playerID == ID {
                Utils.shared.allPlayers[i].currentBetAmount = amount
                Utils.shared.allPlayers[i].currentBetPlayer = onPlayerNum
                print("Player \(ID) is betting \(amount) on Player \(onPlayerNum)")
                return
            }
        }
    }
    
    static func resetAllBets() {
        for player in Utils.shared.allPlayers {
            player.currentBetPlayer = 0
            player.currentBetAmount = 0
        }
    }
    
    static func deleteAllPlayers() {
        for (num, player) in Utils.shared.allPlayers.enumerated() {
            for (i, _) in player.goodOdds.enumerated() {
                UserDefaults.standard.removeObject(forKey: "player\(num)_prize\(i)")
            }
            
            let filename = Utils.shared.getDocumentsDirectory().appendingPathComponent("player\(num).png")
            do {
                try FileManager.default.removeItem(at: filename)
                print("Local path removed successfully")
            } catch let error as NSError {
                print("------Error",error.debugDescription)
            }
        }
        Utils.shared.allPlayers.removeAll()
    }
    
    static func deleteAllTargets() {
        Utils.deleteAllPlayers()
        Utils.shared.allPrizes.removeAll()
    }
    
    func getPlayer(num: Int) -> Player? {
        var moreToSearch = true
        var i = 0
        var odds = [Int]()
        
        while moreToSearch {
            if let odd = UserDefaults.standard.value(forKey: "player\(num)_prize\(i)") as? Int {
                odds.append(odd)
                i += 1
            } else {
                moreToSearch = false
            }
        }
        
        if odds.count > 0 {
            let p = Player()
            p.set(img: getPlayerImage(num), stars: odds, ID: num)
            return p
        } else {
            return nil
        }
    }
    
    func addPlayer(_ image: UIImage, num: Int, odds: [Int]) {
        saveImage(image: image, withName: "player\(num)")
        
        for (i, odd) in odds.enumerated() {
            UserDefaults.standard.set(odd, forKey: "player\(num)_prize\(i)")
        }
        
        let player = Player()
        player.set(img: image, stars: odds, ID: num)
        if num >= Utils.shared.allPlayers.count {
            Utils.shared.allPlayers.append(player)
        } else {
            Utils.shared.allPlayers[num] = player
        }
        
        Database.database().reference().child(Utils.getPlayerNameForID(num)).child("bet").setValue("-")
        Database.database().reference().child(Utils.getPlayerNameForID(num)).child("total").setValue("Fat$ \(Utils.startingCoinAmount)")
    }
    
    func saveImage(image: UIImage, withName: String) {
        if let data = image.pngData() {
            let filename = getDocumentsDirectory().appendingPathComponent("\(withName).png")
            try? data.write(to: filename)
        }
    }
    
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    func getImageFor(name: String) -> UIImage? {
        let path = getDocumentsDirectory().appendingPathComponent("\(name).png")
        let data = try? Data(contentsOf: path)
        if let d = data {
            return UIImage(data: d)
        } else {
            return nil
        }
    }
    
    static func getPlayerNameForID(_ ID: Int) -> String {
        switch ID {
        case 0: return "JD"
        case 1: return "Jess"
        case 2: return "Andrew"
        case 3: return "Shawn"
        case 4: return "Michael"
        case 5: return "Ayesha"
        case 6: return "Bonan"
        case 7: return "Pooja"
        case 8: return "Abi"
        case 9: return "Georgia"
        case 10: return "Dan"
        case 11: return "Kara"
        case 12: return "Claudine"
        case 13: return "Martin"
        case 14: return "Darwin"
        case 15: return "Ben"
        case 16: return "Lenny"
        case 17: return "Ran"
        case 18: return "Carol"
        case 19: return "Brad"
        case 20: return "Yudi"
        case 21: return "Vinnie"
        case 22: return "Adam"
//        case 21: return "<#here#>"
        
        default:
            return "unknown player"
        }
    }
}



extension UIImage {
    func circularImage() -> UIImage? {
        var imageView = UIImageView()
        if self.size.width > self.size.height {
            imageView.frame =  CGRect(x: 0, y: 0, width: self.size.width, height: self.size.width)
            imageView.image = self
            imageView.contentMode = .scaleAspectFit
        } else { imageView = UIImageView(image: self) }
        var layer: CALayer = CALayer()
    
        layer = imageView.layer
        layer.masksToBounds = true
        layer.cornerRadius = self.size.width/2.0
        UIGraphicsBeginImageContext(imageView.bounds.size)
        if let context = UIGraphicsGetCurrentContext() {
            layer.render(in: context)
            let roundedImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
            return roundedImage
        } else {
            return self
        }
    }
}


extension SKSpriteNode {
    func drawBorder(color: UIColor, width: CGFloat, frame f: CGRect) {
        let shapeNode = SKShapeNode(rect: f, cornerRadius: f.size.width/2.0)
        shapeNode.fillColor = .clear
        shapeNode.strokeColor = color
        shapeNode.lineWidth = width
        addChild(shapeNode)
    }
}

