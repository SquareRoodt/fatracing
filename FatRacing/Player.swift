//
//  Player.swift
//  FatRacing
//
//  Created by Jan-Dawid Roodt on 19/02/19.
//  Copyright © 2019 JD. All rights reserved.
//

import UIKit
import SpriteKit

class Player: SKSpriteNode {
    
    static let star1Odds = 33
    static let star2Odds = 66
    static let star3Odds = 80
    
    var image: UIImage?
    var goodOdds: [Int] = [Int]()
    var lastPoint: [Int] = [0,0]
    var playerID: Int = 0
    
    var currentBetPlayer: Int = 0  // Used in cell only [0, 1, 2] from selected Users
    var currentBetAmount: Int = 0  // Used in cell only
    var currentTotalAmount: Int = Utils.startingCoinAmount
    
    func set(img: UIImage?, stars: [Int], ID: Int) {
        for star in stars {
            if star == 1 || star == Player.star1Odds {
                goodOdds.append(Player.star1Odds)
            } else if star == 2 || star == Player.star2Odds {
                goodOdds.append(Player.star2Odds)
            } else if star == 3 || star == Player.star3Odds {
                goodOdds.append(Player.star3Odds)
            } else {
                print("WARNING: no goodOdds set!")
                goodOdds.append(Player.star3Odds)
            }
        }
        
        image = img
        playerID = ID
    }
}
