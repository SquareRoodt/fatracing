//
//  WinnerViewController.swift
//  FatRacing
//
//  Created by Jan-Dawid on 14/3/19.
//  Copyright © 2019 JD. All rights reserved.
//

import UIKit
import Firebase

class WinnerViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    let winnerImage = UIImageView()
    let winnerPrize = UIImageView()
    let winnerLabel = UILabel()
    let tableView = UITableView()
    
    var winningNum: Int = 0
    var players = [Player]()
    var winners = [Player]()
    var losers = [Player]()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.tableView.reloadData()
    }
    
    func setupView() {
        self.view.backgroundColor = .white
        
        let toolButtonW: CGFloat = Utils.isIpad()   ? 180 : 150
        let toolButtonH: CGFloat = Utils.isIpad()   ? 100 : 50
        
        let rightPannel = UIView()
        self.view.addSubview(rightPannel)
        rightPannel.snp.makeConstraints { (make) -> Void in
            make.left.bottom.top.equalToSuperview()
            make.width.equalTo(self.view.snp.width).dividedBy(2)
        }
        
        // Winner Image
        rightPannel.addSubview(winnerImage)
        winnerImage.snp.makeConstraints { (make) -> Void in
            make.top.equalToSuperview().offset(20)
            make.width.height.equalTo(rightPannel.snp.height).multipliedBy(0.6)
            make.centerX.equalToSuperview()
        }
        
        // Winner Prize
        rightPannel.addSubview(winnerPrize)
        winnerPrize.snp.makeConstraints { (make) -> Void in
            make.centerY.equalTo(winnerImage.snp.bottom)
            make.width.height.equalTo(rightPannel.snp.height).multipliedBy(0.4)
            make.centerX.equalTo(winnerImage)
        }
        
        // Winner Label
        winnerLabel.textColor = UIColor.flatYellowDark()
        winnerLabel.textAlignment = .center
        winnerLabel.font = UIFont.systemFont(ofSize: Utils.isIpad() ? 50 : 40, weight: .bold)
        winnerLabel.text = "Winner!"
        rightPannel.addSubview(winnerLabel)
        winnerLabel.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(winnerImage.snp.bottom)
            make.height.equalTo(rightPannel.snp.height).multipliedBy(0.4)
            make.left.right.equalToSuperview()
        }
        
        // TableView
        tableView.register(WinnerTableViewCell.self, forCellReuseIdentifier: "winnerCell")
        tableView.delegate = self
        tableView.dataSource = self
        self.view.addSubview(tableView)
        tableView.snp.makeConstraints { (make) -> Void in
            make.top.equalToSuperview().offset(20)
            make.bottom.equalToSuperview().inset(30 + toolButtonH)
            make.right.equalToSuperview()
            make.left.equalTo(rightPannel.snp.right)
        }
        
        // Done Button
        let doneButton = UIButton()
        doneButton.backgroundColor = UIColor.flatGreen()
        doneButton.setTitleColor(UIColor.white, for: .normal)
        doneButton.setTitle("Continue", for: .normal)
        doneButton.titleLabel?.font = UIFont.systemFont(ofSize: 25, weight: .semibold)
        doneButton.addTarget(self, action: #selector(doneButtonPressed), for: .touchUpInside)
        doneButton.layer.cornerRadius = toolButtonH/2.0
        doneButton.clipsToBounds = true
        self.view.addSubview(doneButton)
        doneButton.snp.makeConstraints { (make) -> Void in
            make.centerX.equalTo(tableView)
            make.bottom.equalToSuperview().offset(-20)
            make.width.equalTo(toolButtonW)
            make.height.equalTo(toolButtonH)
        }
    }
    
    func setWinningNum(_ winningN: Int) {
        self.winningNum = winningN
        sortPlayersByWinnings()
//        print("winner num: \(winningN)")
        self.winnerImage.backgroundColor = UIColor.clear
        self.winnerImage.image = Utils.shared.getPlayerImage(winningN)
        
        winnerPrize.image = Utils.shared.allPrizes[Utils.shared.selectedPrize]
        
//        if Utils.shared.selectedPrize == 0 {
//            winnerPrize.snp.updateConstraints { (make) in
//                make.width.height.equalTo(rightPannel.snp.height).multipliedBy(0.6)
//            }
//        }
    }
    
    func sortPlayersByWinnings() {
        players.removeAll()
        winners.removeAll()
        losers.removeAll()
        
        var winningIndexArrayNum = 0
        
        for (i, p) in Utils.shared.selectedPlayers.enumerated() {
            if p.playerID == winningNum {
                winningIndexArrayNum = i+1
                break
            }
        }
        
        for p in Utils.shared.allPlayers {
//            print("\(p.playerID) bet on player \(p.currentBetPlayer)")
            if p.currentBetPlayer == winningIndexArrayNum {
                winners.append(p)
//                print("player\(p.playerID) wins")
            } else {
                losers.append(p)
            }
        }
        
        winners.sort(by: { $0.currentBetAmount > $1.currentBetAmount })
        losers.sort(by: { $0.currentBetAmount < $1.currentBetAmount })
        
        for p in winners {
            players.append(p)
        }
        for p in losers {
            players.append(p)
        }
        
        self.tableView.reloadData()
    }
    
    func resetPlayerCounts() {
        // Reset all numbers
        
        for player in Utils.shared.allPlayers {
            if winners.contains(player) {
                player.currentTotalAmount -= player.currentBetAmount
                player.currentTotalAmount += player.currentBetAmount * Utils.winningMultiplier
            } else {
                if player.currentTotalAmount - player.currentBetAmount <= 0 {
                    player.currentTotalAmount = 0
                } else {
                    player.currentTotalAmount -= player.currentBetAmount// * Utils.winningMultiplier
                }
            }
            player.currentBetPlayer = 0
            player.currentBetAmount = 0
            
            Database.database().reference().child("\(Utils.getPlayerNameForID(player.playerID))/bet").setValue("-")
            Database.database().reference().child("\(Utils.getPlayerNameForID(player.playerID))/total").setValue("Fat$ \(player.currentTotalAmount)")
        }
        
        
    }
    
    @objc func doneButtonPressed() {
        resetPlayerCounts()
        UIApplication.shared.keyWindow?.rootViewController?.dismiss(animated: false, completion: nil)
    }
    
    // MARK: - Table View Delegates
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Utils.shared.allPlayers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let currentPlayer = self.players[indexPath.row]
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "winnerCell") as! WinnerTableViewCell
        cell.setupWith(image: Utils.shared.getPlayerImage(currentPlayer.playerID),
                       win: winners.contains(currentPlayer),
                       amount: currentPlayer.currentBetAmount)
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return WinnerTableViewCell.cellHeight
    }
    

}
