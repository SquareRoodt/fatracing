//
//  PlayerCell.swift
//  FatRacing
//
//  Created by Jan-Dawid Roodt on 21/02/19.
//  Copyright © 2019 JD. All rights reserved.
//

import UIKit

class PlayerCell: UITableViewCell {
    
    static let suggestedHeight: CGFloat = Utils.isIpad() ? 130 : 60
    
    let currencyUnit = "F$ "
    var profilePicture = UIImageView()
    let label = UILabel()
    let currentAmount = UILabel()
    let button1 = UIButton()
    let button2 = UIButton()
    let button3 = UIButton()
    var player: Player?

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupBaseView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupBaseView() {
        let h: CGFloat = PlayerCell.suggestedHeight - 10
        
        profilePicture.clipsToBounds = true
        profilePicture.layer.cornerRadius = h/2.0
        profilePicture.layer.borderWidth = 5
        profilePicture.layer.borderColor = UIColor.flatGreen().cgColor
        self.contentView.addSubview(profilePicture)
        profilePicture.frame = CGRect(x: 10, y: 5, width: h, height: h)
//        profilePicture.backgroundColor = UIColor.flatGreen()
        
        let textSize: CGFloat = Utils.isIpad() ? 50 : 38
        let buttonFont = UIFont.systemFont(ofSize: textSize, weight: .heavy)
        
        currentAmount.font = buttonFont
        currentAmount.textColor = UIColor.flatGreenDark()
        currentAmount.text = ""
        currentAmount.textAlignment = .left
        self.contentView.addSubview(currentAmount)
        currentAmount.snp.makeConstraints { (make) -> Void in
            make.left.equalTo(profilePicture.snp.right).offset(20)
            make.top.bottom.equalToSuperview()
            make.width.equalTo(Utils.isIpad() ? 250 : 120)
        }
        
        button1.tag = 1
        button1.setTitleColor(UIColor.flatBlack(), for: .normal)
        button1.addTarget(self, action: #selector(buttonPressed(_:)), for: .touchUpInside)
        button1.titleLabel?.font = buttonFont
        button1.titleLabel?.textAlignment = .center
        button1.backgroundColor = UIColor.flatBlue().withAlphaComponent(0.1)
        self.contentView.addSubview(button1)
        button1.snp.makeConstraints { (make) -> Void in
            make.top.bottom.equalToSuperview()
            make.height.width.equalTo(self.snp.height)
            make.right.equalToSuperview().inset(0)
        }
        
        button2.tag = 2
        button2.setTitleColor(UIColor.flatBlack(), for: .normal)
        button2.addTarget(self, action: #selector(buttonPressed(_:)), for: .touchUpInside)
        button2.titleLabel?.font = buttonFont
        button2.titleLabel?.textAlignment = .center
        button2.backgroundColor = UIColor.flatYellow().withAlphaComponent(0.1)
        self.contentView.addSubview(button2)
        button2.snp.makeConstraints { (make) -> Void in
            make.top.bottom.equalToSuperview()
            make.height.width.equalTo(self.snp.height)
            make.right.equalToSuperview().inset(PlayerCell.suggestedHeight)
        }
        
        button3.tag = 3
        button3.setTitleColor(UIColor.flatBlack(), for: .normal)
        button3.addTarget(self, action: #selector(buttonPressed(_:)), for: .touchUpInside)
        button3.titleLabel?.font = buttonFont
        button3.titleLabel?.textAlignment = .center
        button3.backgroundColor = UIColor.flatRed().withAlphaComponent(0.1)
        self.contentView.addSubview(button3)
        button3.snp.makeConstraints { (make) -> Void in
            make.top.bottom.equalToSuperview()
            make.height.width.equalTo(self.snp.height)
            make.right.equalToSuperview().inset(PlayerCell.suggestedHeight * 2)
        }
        
        label.text = "Add Player"
        label.textColor = UIColor.flatGreen()
        label.font = UIFont.systemFont(ofSize: 25, weight: .medium)
        label.textAlignment = .center
        label.isHidden = true
        self.contentView.addSubview(label)
        label.snp.makeConstraints { (make) -> Void in
            make.center.equalToSuperview()
            make.width.equalToSuperview()
            make.height.equalToSuperview()
        }
    }
    
    func setupAsAddition() {
        profilePicture.image = UIImage()
        profilePicture.isHidden = true
        label.isHidden = false
        button1.isHidden = true
        button2.isHidden = true
        button3.isHidden = true
        currentAmount.isHidden = true
    }
    
    func setupWithPlayer(_ player: Player) {
        profilePicture.isHidden = false
        label.isHidden = true
        button1.isHidden = false
        button2.isHidden = false
        button3.isHidden = false
        currentAmount.isHidden = false
        self.player = player
        self.profilePicture.image = player.image
        
        button1.setTitle("", for: .normal)
        button2.setTitle("", for: .normal)
        button3.setTitle("", for: .normal)
        if player.currentBetPlayer == 1 {
            button1.setTitle("\(player.currentBetAmount)", for: .normal)
        } else if player.currentBetPlayer == 2 {
            button2.setTitle("\(player.currentBetAmount)", for: .normal)
        } else if player.currentBetPlayer == 3 {
            button3.setTitle("\(player.currentBetAmount)", for: .normal)
        }
        currentAmount.textColor = UIColor.flatGreenDark()
        if player.currentTotalAmount == 0 && player.currentBetAmount == 0 {
            currentAmount.text = "Bust"
            currentAmount.textColor = UIColor.flatRedDark()
        } else {
            let a = player.currentTotalAmount - player.currentBetAmount
            if a == 0 {
                currentAmount.text = ""
            } else {
                currentAmount.text = "\(currencyUnit) \(a).00"
            }
        }
        
    }
    
    @objc func buttonPressed(_ sender: UIButton) {
        guard let playerID = self.player?.playerID else {
            return
        }
        guard Utils.shared.allPlayers.count > playerID else {
            return
        }
        
        let confirmedPlayer = Utils.shared.allPlayers[playerID]
        
        if confirmedPlayer.currentTotalAmount == 0 && confirmedPlayer.currentBetAmount == 0 {
            return
        }
        
        let currentBetNum = confirmedPlayer.currentBetPlayer
        
        if currentBetNum == 0 {
            Utils.betPlayerID(playerID, amount: 1, onPlayerNum: sender.tag)
            setButton(sender.tag, amount: 1)
            
        } else if currentBetNum == sender.tag {
            let amount = confirmedPlayer.currentBetAmount + 1
            if amount > confirmedPlayer.currentTotalAmount {
                return
            }
            Utils.betPlayerID(playerID, amount: amount, onPlayerNum: sender.tag)
            setButton(sender.tag, amount: amount)
            
        } else {
            Utils.betPlayerID(playerID, amount: 0, onPlayerNum: 0)
            setButton(-1, amount: 0)
        }
    }
    
    func setButton(_ buttonNum: Int, amount: Int) {
        let buttons = [button1, button2, button3]
        
        // Clean All
        guard buttonNum != -1 else {
            for b in buttons {
                b.setTitle("", for: .normal)
                currentAmount.text = "\(currencyUnit) \(self.player?.currentTotalAmount ?? 0).00"
            }
            return
        }
        
        // Set button
        for (i, b) in buttons.enumerated() {
            if i+1 == buttonNum {
                b.setTitle("\(amount)", for: .normal)
            } else {
                b.setTitle("", for: .normal)
            }
        }
        
        if let total = self.player?.currentTotalAmount {
            if total-amount == 0 {
                currentAmount.text = ""
            } else {
                currentAmount.text = "\(currencyUnit) \(total-amount).00"
            }
        }
        
    }

}
